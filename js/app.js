var app = angular.module('musicApp',['ngResource','ngRoute','ngTagsInput']);

app.constant('isLoading',false).constant('endPoints',"").run(function ($rootScope, isLoading) {
    $rootScope.isLoading = isLoading;
});

app.config(function($routeProvider, $locationProvider) {
	  $routeProvider
	  	.when('/', {
			templateUrl: 'tracks/list.html',
			controller: 'TrackListController'
		})
		.when('/tracks', {
			templateUrl: 'tracks/list.html',
			controller: 'TrackListController'
		})
		.when('/tracks/create', {
			templateUrl: 'tracks/track.html',
			controller: 'TrackController'
		})
		.when('/tracks/:trackID', {
			templateUrl: 'tracks/view.html',
			controller: 'TrackViewController'
		})
		.when('/tracks/:trackID/edit', {
			templateUrl: 'tracks/track.html',
			controller: 'TrackController'
		})
		.when('/genres', {
			templateUrl: 'genres/list.html',
			controller: 'GenreListController'
		})
		.when('/genres/create', {
			templateUrl: 'genres/genre.html',
			controller: 'GenreController'
		})
		.when('/genres/:genreID/edit', {
			templateUrl: 'genres/genre.html',
			controller: 'GenreController'
		})
		.when('/genres/:genreID', {
			templateUrl: 'genres/view.html',
			controller: 'GenreViewController'
		});
	})
	.factory('Track', function($resource, endPoints) {
		return $resource(endPoints+'tracks/:id', { id: '@id' });
	})
	.factory('Genre', function($resource, endPoints) {
		return $resource(endPoints+'genres/:id', { id: '@id' });
	});



app.controller('TrackListController', ['$rootScope','$scope', '$timeout','Track','isLoading', function($rootScope,$scope, $timeout, Track, isLoading) {
	$scope.loadSize = 5;
	$scope.tracks = [];
 	$scope.trackName = "";
 	var timeoutForSearch = null;


 	$scope.filterTracks = function (trackName) {
 		$rootScope.isLoading = true;

 		if(timeoutForSearch) {
 			$timeout.cancel(timeoutForSearch);
 		}
 		
 		timeoutForSearch = $timeout(function() {
	 		$scope.tracks = Track.query({title: trackName}, function () {
 				$rootScope.isLoading = false;
	 		});
        }, 300);
 	}

 	$scope.filterTracks($scope.trackName);
}]);

app.controller('TrackController', ['$rootScope','$scope','$routeParams','$location','Track','Genre', function($rootScope,$scope, $routeParams,$location, Track, Genre) {
	
	$scope.track = new Track();
	$scope.genres = Genre.query();

	if($routeParams.trackID){
 		$rootScope.isLoading = true;

		$scope.track = Track.get({ id: $routeParams.trackID }, function (track) {
 			$rootScope.isLoading = false;

			setTimeout(function () {
				$("#genre").trigger("change");
			}, 500);
		});
	} else {
		$scope.track.title = "";
		$scope.track.rating = 0;
		$scope.track.genres = [];
	}
	
	$("#genre").select2({width: "100%"}).on("change", function () {
		$scope.track.genres = $(this).val();
	});

	$scope.saveTrack = function () {
		if($scope.track.title){
			$rootScope.isLoading = true;

			Track.save($scope.track, function(track) {
				$rootScope.isLoading = false;

		    	$location.path('/tracks/'+track.id);
		  	});
		} else {
			alert("Please enter track name.");
		}
	}
}]);

app.controller('TrackViewController', ['$rootScope','$scope','$routeParams','Track','Genre', function($rootScope,$scope, $routeParams, Track, Genre) {
	$rootScope.isLoading = true;

	$scope.track = Track.get({ id: $routeParams.trackID }, function  () {
		$rootScope.isLoading = false;
	});
}]);

app.controller('GenreListController', ['$rootScope','$scope', '$timeout','Genre', function($rootScope,$scope, $timeout, Genre) {
	$scope.loadSize = 5;
	$scope.genres = [];
 	$scope.genreName = "";
 	var timeoutForSearch = null;


 	$scope.filterGenres = function (genreName) {
 		$rootScope.isLoading = true;

 		if(timeoutForSearch) {
 			$timeout.cancel(timeoutForSearch);
 		}
 		
 		timeoutForSearch = $timeout(function() {
	 		$scope.genres = Genre.query({title: genreName}, function () {
				$rootScope.isLoading = false;
	 		});
        }, 300);
 	}

 	$scope.filterGenres($scope.genreName);
}]);

app.controller('GenreController', ['$rootScope','$scope','$routeParams','$location','Genre', function($rootScope,$scope, $routeParams,$location, Genre) {
	
	if($routeParams.genreID){
		$rootScope.isLoading = true;

		$scope.genre = Genre.get({ id: $routeParams.genreID }, function () {
			$rootScope.isLoading = false;
		});
	} else {
		$scope.genre = new Genre();
		$scope.genre.name = "";
	}
	
	$scope.saveGenre = function () {
		if($scope.genre.name){
			$rootScope.isLoading = true;

			Genre.save($scope.genre, function(genre) {
				$rootScope.isLoading = false;

		    	$location.path('/genres/'+genre.id);
		  	});
		} else {
			alert("Please enter genre name.");
		}
	}
}]);

app.controller('GenreViewController', ['$rootScope','$scope','$routeParams', 'Genre', function($rootScope,$scope, $routeParams, Genre) {
	$rootScope.isLoading = true;

	$scope.genre = Genre.get({ id: $routeParams.genreID }, function () {
		$rootScope.isLoading = false;
	});
}]);

app.directive('rating', function () {
    return {
        scope: {rate: '='},
        template: [
        	"<div class='rating'>",
        		'<span class="glyphicon" ng-class="getClass(star)" ng-repeat="star in stars"></span>',
        	"</div>"
        ].join(""),
        link : function (scope, elem, attr) {
		  	scope.stars = [];
			
			for ( var i = 1; i <= 5; i++) {
			   scope.stars.push({
			   		filled : i <= scope.rate
			   });
			}

			scope.getClass = function (star) {
				return star.filled ? "glyphicon-star":"glyphicon-star-empty";
			}

			scope.$watch('rate', function(newVal) {
				scope.stars = [];
			
				for ( var i = 1; i <= 5; i++) {
				   scope.stars.push({
				   		filled : i <= newVal
				   });
				}
			});
		}
    };
});

app.directive('rateEditor', function () {
    return {
        scope: {rate: '='},
        template: [
        	"<div class='rating'>",
        		'<span class="glyphicon" ng-click="applyRating($index)" ng-class="getClass(star)" ng-repeat="star in stars"></span>',
        	"</div>"
        ].join(""),
        link : function (scope, elem, attr) {
		  	scope.stars = [];
			
			for ( var i = 1; i <= 5; i++) {
			   scope.stars.push({
			   		filled : i <= scope.rate
			   });
			}

			scope.getClass = function (star) {
				return star.filled ? "glyphicon-star":"glyphicon-star-empty";
			}
			
			scope.applyRating = function (index) {
				scope.rate = index + 1;
			}

			scope.$watch('rate', function(newVal) {
				scope.stars = [];
			
				for ( var i = 1; i <= 5; i++) {
				   scope.stars.push({
				   		filled : i <= newVal
				   });
				}
			});
		}
    };
});